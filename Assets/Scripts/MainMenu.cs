﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public AudioSource src;
    public void Play()
    {
        SceneManager.LoadScene(1);
    }

    public void Mute()
    {
        src.mute = true;
    }
}
